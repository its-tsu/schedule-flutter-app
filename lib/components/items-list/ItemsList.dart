import 'package:flutter/cupertino.dart';
import 'package:flutter_app/components/items-list/DropdownList.dart';
import 'package:flutter_app/components/shared/ConnectionError.dart';
import 'package:flutter_app/components/shared/Loader.dart';
import 'package:flutter_app/data/ResponseTypesEnum.dart';
import 'package:flutter_app/models/GroupItems.dart';
import 'package:flutter_app/services/api/GroupItemsApiService.dart';

class ItemsList extends StatefulWidget {
  final ResponseTypesEnum type;
  final int rootItemId;

  ItemsList({Key key, this.type, this.rootItemId}) : super(key: key);

  @override
  _ItemsListState createState() => _ItemsListState(this.type, this.rootItemId);
}

class _ItemsListState extends State<ItemsList> {
  final ResponseTypesEnum type;
  final int rootItemId;
  GroupItemsApiService groupItemsApiService;
  Future<List<GroupItems>> futureList;

  _ItemsListState(this.type, this.rootItemId) {
    this.groupItemsApiService = GroupItemsApiService();
  }

  @override
  void initState() {
    super.initState();
    futureList = this.groupItemsApiService.getGroups(this.rootItemId, type);
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<List<GroupItems>>(
      future: this.futureList,
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          return ListView.builder(
              padding: EdgeInsets.only(bottom: 60),
              itemCount: snapshot.data.length,
              itemBuilder: (context, index) {
                return Container(
                  margin: EdgeInsets.only(top: 20),
                  child: DropdownList(
                      itemGroup: snapshot.data[index].group,
                      itemsList: snapshot.data[index].itemsList,
                      type: this.type
                  ),
                );
              });
        }

        if (snapshot.hasError) {
          return ConnectionError();
        }

        return Loader();
      },
    );
  }
}
