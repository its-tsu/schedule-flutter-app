import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/data/Paths.dart';
import 'package:flutter_app/data/ResponseTypesEnum.dart';
import 'package:flutter_app/models/BaseModel.dart';
import 'package:flutter_app/models/screen-arguments/ScheduleViewArguments.dart';

class DropdownItem extends StatelessWidget {
  final BaseModel item;
  final ResponseTypesEnum type;

  DropdownItem({ Key key, this.item, this.type }): super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      child: DecoratedBox(
        decoration: BoxDecoration(color: Colors.white),
        child: Theme(
          data: Theme.of(context).copyWith(
              buttonTheme: ButtonTheme.of(context).copyWith(
                  materialTapTargetSize: MaterialTapTargetSize.shrinkWrap)),
          child: OutlineButton(
            onPressed: () {
              Navigator.pushNamed(context, this._getLink(),
                  arguments:
                  ScheduleViewArguments(item: item, type: type));
            },
            color: Colors.white,
            padding: EdgeInsets.all(15),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Text(item.name,
                    textAlign: TextAlign.left,
                    style: TextStyle(
                      color: Colors.black,
                    ))
              ],
            ),
          ),
        ),
      ),
    );
  }

  String _getLink() {
    if (type == ResponseTypesEnum.students) {
      return Paths.studentsSchedule;
    }

    if (type == ResponseTypesEnum.teachers) {
      return Paths.teachersSchedule;
    }

    if (type == ResponseTypesEnum.classRooms) {
      return Paths.classroomsSchedule;
    }

    return "/";
  }

}