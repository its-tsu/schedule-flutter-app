import 'dart:math';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/components/items-list/DropdownItem.dart';
import 'package:flutter_app/data/ResponseTypesEnum.dart';
import 'package:flutter_app/models/BaseModel.dart';

class DropdownList extends StatefulWidget {
  final BaseModel itemGroup;
  final List<BaseModel> itemsList;
  final ResponseTypesEnum type;

  DropdownList({this.itemGroup, this.itemsList, this.type});

  @override
  _DropdownListState createState() => _DropdownListState();
}

class _DropdownListState extends State<DropdownList>
    with TickerProviderStateMixin {
  bool isVisible = false;

  AnimationController _rotationAnimationController;
  AnimationController _reversedRotationAnimationController;
  Animation<double> _rotationAnimation;
  Animation<double> _reversedRotationAnimation;
  Animation animation;

  @override
  void initState() {
    super.initState();

    _rotationAnimationController =
        AnimationController(vsync: this, duration: Duration(milliseconds: 500));
    _reversedRotationAnimationController =
        AnimationController(vsync: this, duration: Duration(milliseconds: 500));

    _rotationAnimation =
        Tween<double>(begin: pi, end: 0).animate(_rotationAnimationController);
    _reversedRotationAnimation = Tween<double>(begin: 0, end: pi)
        .animate(_reversedRotationAnimationController);

    this.animation = this._reversedRotationAnimation;
  }

  @override
  void dispose() {
    _rotationAnimationController?.dispose();
    _reversedRotationAnimationController?.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          DecoratedBox(
            decoration: BoxDecoration(color: Colors.white),
            child: Theme(
              data: Theme.of(context).copyWith(
                  buttonTheme: ButtonTheme.of(context).copyWith(
                      materialTapTargetSize: MaterialTapTargetSize.shrinkWrap)),
              child: OutlineButton(
                onPressed: this._changeDropdownState,
                color: Colors.white,
                padding: EdgeInsets.all(15),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Expanded(
                        flex: 9,
                        child: Text(widget.itemGroup.name,
                            textAlign: TextAlign.left,
                            style: TextStyle(
                              color: Colors.black,
                            ))),
                    Expanded(
                        flex: 1,
                        child: AnimatedBuilder(
                            animation: this.animation,
                            child: Icon(Icons.keyboard_arrow_down),
                            builder: (context, child) {
                              return Transform.rotate(
                                angle: this.animation.value,
                                child: child,
                              );
                            }))
                  ],
                ),
              ),
            ),
          ),
          isVisible
              ? Column(children: widget.itemsList.map((item) => DropdownItem(item: item, type: widget.type)).toList())
              : Container()
        ],
      ),
    );
  }

  void _changeDropdownState() {
    setState(() {
      isVisible = !isVisible;

      if (isVisible) {
        _reversedRotationAnimationController.forward(from: 0);
      } else {
        _rotationAnimationController.forward(from: 0);
      }

      this.animation = this._buildAnimation();
    });
  }

  Animation _buildAnimation() {
    if (isVisible) {
      return _reversedRotationAnimation;
    } else {
      return _rotationAnimation;
    }
  }
}
