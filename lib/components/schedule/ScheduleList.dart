import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/components/schedule/Calendar.dart';
import 'package:flutter_app/components/schedule/SchedulePair.dart';
import 'package:flutter_app/components/shared/ConnectionError.dart';
import 'package:flutter_app/components/shared/Loader.dart';
import 'package:flutter_app/models/ClassModel.dart';
import 'package:flutter_app/models/LatestItemsModel.dart';
import 'package:flutter_app/models/screen-arguments/ScheduleViewArguments.dart';
import 'package:flutter_app/services/api/ScheduleApiService.dart';
import 'package:flutter_app/services/offline/LatestItemsService.dart';
import 'package:flutter_app/utils/Date.dart';

class ScheduleList extends StatefulWidget {
  final ScheduleViewArguments arguments;

  ScheduleList(this.arguments);

  @override
  _ScheduleListState createState() => _ScheduleListState();
}

class _ScheduleListState extends State<ScheduleList> {
  DateTime selectedDate;

  ScheduleApiService scheduleApiService;
  Future<List<List<ClassModel>>> futureList;

  ScrollController _scrollController;
  LatestItemsService _latestItemsService;

  _ScheduleListState() {
    this.selectedDate = DateTime.now();
    this.scheduleApiService = ScheduleApiService();
  }

  @override
  void initState() {
    super.initState();

    _scrollController = ScrollController();

    futureList = this.scheduleApiService.getScheduleByTypeAndDates(
          type: widget.arguments.type,
          itemId: widget.arguments.item.id,
          fromDate: Date.getDateStringForRequest(selectedDate),
          toDate: Date.getDateStringForRequest(selectedDate),
        );

    var item = LatestItemsModel(
        id: widget.arguments.item.id,
        name: widget.arguments.item.name,
        typeString: LatestItemsModel.getType(widget.arguments.type),
        lastVisitDate: Date.getDateStringForRequest(DateTime.now()),
        lastUpdatedDate: "");
    this._latestItemsService = LatestItemsService();
    this._latestItemsService.updateItems(item);
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  onDateClick(DateTime date) {
    setState(() {
      this.selectedDate = date;
      futureList = this.scheduleApiService.getScheduleByTypeAndDates(
            type: widget.arguments.type,
            itemId: widget.arguments.item.id,
            fromDate: Date.getDateStringForRequest(selectedDate),
            toDate: Date.getDateStringForRequest(selectedDate),
          );
      this._scrollToTop();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: <Widget>[
          Calendar(
            selectedDate: selectedDate,
            onDateClick: onDateClick,
          ),
          Container(
            child: FutureBuilder<List<List<ClassModel>>>(
                future: this.futureList,
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    return Expanded(
                      child: ListView.builder(
                          controller: this._scrollController,
                          padding: EdgeInsets.only(top: 20, bottom: 60),
                          itemCount: snapshot.data.length,
                          itemBuilder: (context, index) {
                            return snapshot.data[index].length > 0
                                ? SchedulePair(
                                    classesList: snapshot.data[index],
                                    arguments: widget.arguments,
                                    isFirst: index == 0)
                                : Container(
                                    height: 100,
                                    decoration: BoxDecoration(
                                        border: Border(
                                            top: BorderSide(
                                                color: Colors.black, width: index == 0 ? 1 : 0),
                                            bottom: BorderSide(
                                                color: Colors.black,
                                                width: 1))),
                                    child: Center(
                                      child: Text(
                                        "Нет пары",
                                        style: TextStyle(
                                            fontSize: 14, color: Colors.black),
                                      ),
                                    ),
                                  );
                          }),
                    );
                  }

                  if (snapshot.hasError) {
                    return ConnectionError();
                  }

                  return Loader();
                }),
          )
        ],
      ),
    );
  }

  _scrollToTop() {
    _scrollController.animateTo(_scrollController.position.minScrollExtent,
        duration: Duration(milliseconds: 1000), curve: Curves.easeIn);
  }
}
