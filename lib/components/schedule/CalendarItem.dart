import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/utils/Date.dart';

class CalendarItem extends StatelessWidget {
  final bool active;
  final DateTime item;
  final void Function(DateTime) onClick;

  CalendarItem({ Key key, bool active, this.item, this.onClick })
      : this.active = active == null ? false : active,
        super(key: key);


  @override
  Widget build(BuildContext context) {
    return Container(
      width: 50,
      height: 50,
      decoration: BoxDecoration(
          shape: BoxShape.circle,
          color: active ? Color.fromRGBO(90, 130, 223, 1.0) : Colors.transparent),
      child: Center(
        child: FlatButton(
            onPressed: () {
              this.onClick(this.item);
            },
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(Date.getWeekDay(this.item),
                    style: TextStyle(color: active ? Colors.white : Colors.black)),
                Text(this.item.day.toString(),
                    style: TextStyle(color: active ? Colors.white : Colors.black))
              ],
            )),
      ),
    );
  }
}