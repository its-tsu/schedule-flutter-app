import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/components/schedule/CalendarItem.dart';
import 'package:flutter_app/utils/Date.dart';

class Calendar extends StatefulWidget {
  final DateTime selectedDate;
  final void Function(DateTime) onDateClick;

  Calendar({Key key, this.selectedDate, this.onDateClick}) : super(key: key);

  @override
  _CalendarState createState() => _CalendarState();
}

class _CalendarState extends State<Calendar> {
  DateTime _selectedWeekStart;
  List<DateTime> _selectedWeekDays = [];
  List<Widget> _weekList = [];

  @override
  void initState() {
    super.initState();
    Date.initLocale();

    this._selectedWeekStart = widget.selectedDate
        .subtract(Duration(days: widget.selectedDate.weekday - 1));
    for (var i = 0; i < 6; i++) {
      this
          ._selectedWeekDays
          .add(this._selectedWeekStart.add(Duration(days: i)));
    }

    setState(() {
      this._weekList = this.getCalendarItems();
    });
  }

  @override
  void didUpdateWidget(Widget oldWidget) {
    super.didUpdateWidget(oldWidget);

    this._selectedWeekStart = widget.selectedDate
        .subtract(Duration(days: widget.selectedDate.weekday - 1));

    this._selectedWeekDays = [];

    for (var i = 0; i < 6; i++) {
      this
          ._selectedWeekDays
          .add(this._selectedWeekStart.add(Duration(days: i)));
    }

    setState(() {
      this._weekList = this.getCalendarItems();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: <Widget>[
          Container(
            height: 40,
            width: double.infinity,
            padding: EdgeInsets.only(left: 20, right: 20),
            color: Color.fromRGBO(90, 130, 223, 1.0),
            child: Row(
              children: <Widget>[
                Center(
                  child: Text(
                    Date.getMonth(this._selectedWeekStart),
                    style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.bold,
                        color: Colors.white),
                  ),
                )
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.only(left: 5, right: 5),
            height: 60,
            color: Color.fromRGBO(196, 211, 246, 1.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: this._weekList,
            ),
          ),
          Container(
            height: 40,
            width: double.infinity,
            color: Color.fromRGBO(90, 130, 223, 1.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                FlatButton(
                  onPressed: () {
                    this._onPrevClick();
                  },
                  child: Row(
                    children: <Widget>[
                      Icon(Icons.keyboard_arrow_left,
                          size: 20, color: Colors.white),
                      Text(
                        "Предыдущая неделя",
                        style: TextStyle(
                            fontSize: 12,
                            fontWeight: FontWeight.bold,
                            color: Colors.white),
                      )
                    ],
                  ),
                ),
                Container(
                  height: 24,
                  width: 2,
                  color: Colors.white,
                ),
                FlatButton(
                  onPressed: () {
                    this._onNextClick();
                  },
                  child: Row(
                    children: <Widget>[
                      Text(
                        "Следующая неделя",
                        style: TextStyle(
                            fontSize: 12,
                            fontWeight: FontWeight.bold,
                            color: Colors.white),
                      ),
                      Icon(Icons.keyboard_arrow_right,
                          size: 20, color: Colors.white)
                    ],
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }

  List<Widget> getCalendarItems() {
    return this
        ._selectedWeekDays
        .map((element) => CalendarItem(
              active: Date.isTheSame(element, widget.selectedDate),
              item: element,
              onClick: widget.onDateClick,
            ))
        .toList();
  }

  void _onPrevClick() {
    this._selectedWeekStart =
        this._selectedWeekStart.subtract(Duration(days: 7));

    this._selectedWeekDays = [];

    for (var i = 0; i < 6; i++) {
      this
          ._selectedWeekDays
          .add(this._selectedWeekStart.add(Duration(days: i)));
    }

    setState(() {
      this._weekList = this.getCalendarItems();
    });
  }

  void _onNextClick() {
    this._selectedWeekStart =
        this._selectedWeekStart.subtract(Duration(days: -7));

    this._selectedWeekDays = [];

    for (var i = 0; i < 6; i++) {
      this
          ._selectedWeekDays
          .add(this._selectedWeekStart.add(Duration(days: i)));
    }

    setState(() {
      this._weekList = this.getCalendarItems();
    });
  }
}
