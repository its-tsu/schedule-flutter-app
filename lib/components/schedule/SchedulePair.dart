import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/data/ResponseTypesEnum.dart';
import 'package:flutter_app/models/BaseModel.dart';
import 'package:flutter_app/models/ClassModel.dart';
import 'package:flutter_app/models/screen-arguments/ScheduleViewArguments.dart';
import 'package:flutter_app/utils/Date.dart';

class SchedulePair extends StatelessWidget {
  final bool isFirst;
  final List<ClassModel> classesList;
  final ScheduleViewArguments arguments;

  SchedulePair({ Key key, this.isFirst, this.classesList, this.arguments }): super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(20),
      decoration: BoxDecoration(
          border: Border(
              top: BorderSide(color: Colors.black, width: isFirst ? 1 : 0),
              bottom: BorderSide(color: Colors.black, width: 1))),
      child: Column(
        children: classesList
            .map((classItem) => Container(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                "${classItem.disciplineName} (${classItem.type}.)",
                style: TextStyle(
                    fontSize: 14,
                    fontWeight: FontWeight.bold,
                    color: Colors.black),
              ),
              Container(
                margin: EdgeInsets.only(top: 10, bottom: 10),
                child: Wrap(
                  children: classItem.groupsList
                      .map((group) => Text(
                    group.name + " ",
                    style: TextStyle(
                        fontSize: 12,
                        color: this._getGroupNameColor(group)),
                  ))
                      .toList(),
                ),
              ),
              Text(
                  classItem.teacher != null
                      ? "${classItem.teacher.lastName} ${classItem.teacher.name[0]}.${classItem.teacher.patronymic[0]}."
                      : "",
                  style: TextStyle(
                      fontSize: 12,
                      fontWeight: FontWeight.bold,
                      color: Colors.black)),
              Container(
                margin: EdgeInsets.only(top: 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Text("аудитория: ",
                            style: TextStyle(
                                fontSize: 12,
                                fontWeight: FontWeight.w300,
                                color: Colors.black)),
                        Text(
                            classItem.classroom != null
                                ? classItem.classroom.name
                                : "",
                            style: TextStyle(
                                fontSize: 12,
                                color:
                                Color.fromRGBO(50, 88, 178, 1.0)))
                      ],
                    ),
                    Text(
                        "${classItem.pairNumber} Пара ${this._getPairTime(classItem.fromTime, classItem.toTime)}")
                  ],
                ),
              )
            ],
          ),
        ))
            .toList(),
      ),
    );
  }

  Color _getGroupNameColor(BaseModel group) {
    return this.arguments.type == ResponseTypesEnum.students &&
        this.arguments.item.id == group.id
        ? Color.fromRGBO(50, 88, 178, 1.0)
        : Colors.black;
  }

  String _getPairTime(String fromDate, String toDate) {
    return "[${Date.getTimeFromStringDate(fromDate)} - ${Date.getTimeFromStringDate(toDate)}]";
  }


}