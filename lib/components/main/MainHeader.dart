import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/components/shared/SearchButton.dart';

class MainHeader extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.transparent,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          RotatedBox(
            quarterTurns: 2,
            child: IconButton(
              onPressed: () {
                Navigator.pop(context);
              },
              icon: Icon(Icons.keyboard_tab, color: Colors.white),
              iconSize: 24,
            ),
          ),
          Container(
            width: 150,
            height: 34,
            child: Image.asset("images/tsu-logo.png"),
          ),
          SearchButton(),
        ],
      ),
    );
  }
}
