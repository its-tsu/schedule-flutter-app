import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/data/Paths.dart';
import 'package:flutter_app/data/ResponseTypesEnum.dart';
import 'package:flutter_app/models/BaseModel.dart';
import 'package:flutter_app/models/LatestItemsModel.dart';
import 'package:flutter_app/models/screen-arguments/ScheduleViewArguments.dart';

class CarouselItem extends StatelessWidget {
  final LatestItemsModel item;

  CarouselItem({ Key key, this.item }): super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 150,
      height: 60,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(3)),
        color: Colors.white,
      ),
      child: FlatButton(
          onPressed: () {
            Navigator.pushNamed(context, this._getLink(this.item),
                arguments: ScheduleViewArguments(
                    item: BaseModel(id: this.item.id, name: this.item.name),
                    type: this._getNavigatorType(this.item)));
          },
          child: Center(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  this._getItemTypeText(this.item.typeString),
                  textAlign: TextAlign.left,
                  style: TextStyle(
                      fontSize: 12,
                      color: Color.fromRGBO(184, 192, 219, 1)),
                ),
                Container(
                  margin: EdgeInsets.only(top: 4),
                  child: Text(
                    this.item.name,
                    style: TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.bold,
                        color: Color.fromRGBO(90, 130, 223, 1)),
                  ),
                )
              ],
            ),
          )),
    );
  }

  String _getItemTypeText(String type) {
    if (type == "students") {
      return "Группа";
    }

    if (type == "teachers") {
      return "Преподаватель";
    }

    if (type == "classRooms") {
      return "Аудитория";
    }

    return "";
  }

  String _getLink(LatestItemsModel item) {
    if (item.typeString == "students") {
      return Paths.studentsSchedule;
    }

    if (item.typeString == "teachers") {
      return Paths.teachersSchedule;
    }

    if (item.typeString == "classRooms") {
      return Paths.classroomsSchedule;
    }

    return "/";
  }

  ResponseTypesEnum _getNavigatorType(LatestItemsModel item) {
    if (item.typeString == "students") {
      return ResponseTypesEnum.students;
    }

    if (item.typeString == "teachers") {
      return ResponseTypesEnum.teachers;
    }

    if (item.typeString == "classRooms") {
      return ResponseTypesEnum.classRooms;
    }

    return ResponseTypesEnum.students;
  }

}