import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter_app/components/main/CarouselItem.dart';
import 'package:flutter_app/components/shared/Loader.dart';
import 'package:flutter_app/models/LatestItemsModel.dart';
import 'package:flutter_app/services/offline/LatestItemsService.dart';

class LatestItemsList extends StatefulWidget {
  @override
  _LatestItemsListState createState() => _LatestItemsListState();
}

class _LatestItemsListState extends State<LatestItemsList> {
  LatestItemsService _latestItemsService;
  Future<List<LatestItemsModel>> _futureList;

  _LatestItemsListState() {
    this._latestItemsService = LatestItemsService();
  }

  @override
  void initState() {
    super.initState();
    this._futureList = this._latestItemsService.getLatestItems();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: <Widget>[
          Container(
            padding: EdgeInsets.only(left: 20, right: 20),
            child: Row(
              children: <Widget>[
                Text(
                  "Последние просмотры",
                  style: TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.bold,
                    color: Colors.white,
                  ),
                )
              ],
            ),
          ),
          FutureBuilder<List<LatestItemsModel>>(
              future: this._futureList,
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  if (snapshot.data.length > 0) {
                    return Container(
                      margin: EdgeInsets.only(top: 20),
                      padding: EdgeInsets.only(left: 20, right: 20),
                      child: CarouselSlider(
                          items: _getCarouselItems(snapshot.data),
                          options: _getCarouselOptions()),
                    );
                  } else {
                    return Container(
                      margin: EdgeInsets.only(top: 20),
                      padding: EdgeInsets.only(left: 20, right: 20),
                      child: Text(
                        "Здесь отобразятся последние посещенные страницы",
                        style: TextStyle(
                          fontSize: 14,
                          fontWeight: FontWeight.bold,
                          color: Colors.white,
                        ),
                      ),
                    );
                  }
                }

                if (snapshot.hasError) {
                  return Text(
                    "Не удалось получить доступ к файловой системе",
                    style: TextStyle(
                      fontSize: 14,
                      fontWeight: FontWeight.bold,
                      color: Colors.white,
                    ),
                  );
                }

                return Loader(color: Colors.white);
              })
        ],
      ),
    );
  }

  CarouselOptions _getCarouselOptions() {
    return CarouselOptions(
        height: 60, viewportFraction: 1, enableInfiniteScroll: true);
  }

  List<Widget> _getCarouselItems(List<LatestItemsModel> latestItemsList) {
    var displayList = this._getDisplayList(latestItemsList);
    List<Widget> slidesList = [];

    displayList.forEach((subList) {
      if (subList.length == 0) return;

      slidesList.add(Container(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: subList.map((e) {
            return CarouselItem(item: e);
          }).toList(),
        ),
      ));
    });

    return slidesList;
  }

  List<List<LatestItemsModel>> _getDisplayList(List<LatestItemsModel> latestItemsList) {
    List<List<LatestItemsModel>> displayList = [[], [], []];
    int index = 0;
    int subIndex = 0;

    latestItemsList.forEach((element) {
      displayList[subIndex].add(element);

      if (index % 2 == 1) {
        subIndex++;
      }

      index++;
    });

    return displayList;
  }
}
