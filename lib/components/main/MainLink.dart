import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class MainLink extends StatelessWidget {
  final IconData icon;
  final String text;
  final String link;

  const MainLink({Key key, this.icon, this.text, this.link}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FlatButton(
        onPressed: () {
          Navigator.pushNamed(context, this.link);
        },
        padding: EdgeInsets.only(bottom: 20.0),
        child: Container(
          width: 150,
          height: 90,
          padding: EdgeInsets.all(10.0),
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.all(Radius.circular(6)),
            boxShadow: <BoxShadow>[
              BoxShadow(
                color: Color.fromRGBO(67, 106, 246, 0.35),
                offset: Offset(0.0, 4.0),
                blurRadius: 40.0,
              ),
            ],
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Icon(this.icon, size: 48, color: Color.fromRGBO(50, 88, 178, 1)),
              Text(
                this.text,
                style: TextStyle(fontSize: 14, color: Color.fromRGBO(50, 88, 178, 1)),
              )
            ],
          ),
        ));
  }
}
