import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/data/Paths.dart';
import 'package:flutter_app/data/ResponseTypesEnum.dart';
import 'package:flutter_app/models/SearchItem.dart';
import 'package:flutter_app/models/screen-arguments/ScheduleViewArguments.dart';
import 'package:flutter_app/services/api/SearchApiService.dart';

class SearchContainer extends StatefulWidget {
  @override
  _SearchContainerState createState() => _SearchContainerState();
}

class _SearchContainerState extends State<SearchContainer> {
  Future<List<SearchItem>> futureList;
  SearchApiService searchApiService;

  TextEditingController _searchQuery;

  _SearchContainerState() {
    searchApiService = SearchApiService();
  }

  @override
  void initState() {
    super.initState();

    _searchQuery = TextEditingController();
    futureList = searchApiService.getAllItemsByName("");
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: BackButton(
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        title: this._buildSearchField(),
      ),
      body: Container(
        child: FutureBuilder<List<SearchItem>>(
            future: futureList,
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                return ListView.builder(
                    itemCount: snapshot.data.length,
                    itemBuilder: (context, index) {
                      return Container(
                        decoration: BoxDecoration(
                            border: Border(
                                bottom:
                                    BorderSide(color: Colors.black, width: 1))),
                        child: FlatButton(
                            onPressed: () {
                              Navigator.pushNamed(
                                  context, this._getLink(snapshot.data[index]),
                                  arguments: ScheduleViewArguments(
                                      item: snapshot.data[index],
                                      type: snapshot.data[index].type));
                            },
                            child: Text(
                              snapshot.data[index].name,
                              textAlign: TextAlign.left,
                              style: TextStyle(color: Colors.black),
                            )),
                      );
                    });
              }

              if (snapshot.hasError) {
                return Text(snapshot.error);
              }

              return Text("Loading...");
            }),
      ),
    );
  }

  Widget _buildSearchField() {
    return TextField(
      controller: _searchQuery,
      autofocus: true,
      decoration: const InputDecoration(
        hintText: 'Search...',
        border: InputBorder.none,
        hintStyle: const TextStyle(color: Colors.white30),
      ),
      style: const TextStyle(color: Colors.white, fontSize: 16.0),
      onChanged: this._onInputChange,
    );
  }

  void _onInputChange(String value) {
    setState(() {
      futureList = searchApiService.getAllItemsByName(value);
    });
  }

  String _getLink(SearchItem item) {
    if (item.type == ResponseTypesEnum.students) {
      return Paths.studentsSchedule;
    }

    if (item.type == ResponseTypesEnum.teachers) {
      return Paths.teachersSchedule;
    }

    if (item.type == ResponseTypesEnum.classRooms) {
      return Paths.classroomsSchedule;
    }

    return "/";
  }
}
