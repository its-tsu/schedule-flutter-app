import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/data/Paths.dart';

class SharedBottomNavigationBar extends StatefulWidget
    implements PreferredSizeWidget {

  @override
  Size get preferredSize => Size.fromHeight(40);

  _SharedBottomNavigationBarState createState() => _SharedBottomNavigationBarState();
}

class _SharedBottomNavigationBarState extends State<SharedBottomNavigationBar> {
  int _selectedIndex = 0;

  int _getItemIndexByRoute(BuildContext context) {
    String path = ModalRoute.of(context).settings.name;

    if (path.contains(Paths.teachersRoot)) {
      return 1;
    }

    if (path.contains(Paths.classroomsRoot)) {
      return 2;
    }

    return 0;
  }

  void _onItemTapped(int index) {
    if (index == 0) {
      Navigator.pushNamed(context, Paths.studentsRoot);
    }

    if (index == 1) {
      Navigator.pushNamed(context, Paths.teachersRoot);
    }

    if (index == 2) {
      Navigator.pushNamed(context, Paths.classroomsRoot);
    }
  }

  @override
  Widget build(BuildContext context) {

    _selectedIndex = _getItemIndexByRoute(context);

    return BottomNavigationBar(
      backgroundColor: Color.fromRGBO(90, 130, 223, 1.0),
      showSelectedLabels: false,
      showUnselectedLabels: false,
      items: <BottomNavigationBarItem>[
        BottomNavigationBarItem(
            icon: Icon(Icons.school, size: 24, color: Colors.white),
            activeIcon: Icon(Icons.school, size: 24, color: Color.fromRGBO(58, 81, 153, 1.0)),
            title: Text("")),
        BottomNavigationBarItem(
            icon: Icon(Icons.collections_bookmark, size: 24, color: Colors.white),
            activeIcon: Icon(Icons.collections_bookmark, size: 24, color: Color.fromRGBO(58, 81, 153, 1.0)),
            title: Text("")),
        BottomNavigationBarItem(
            icon: Icon(Icons.account_balance, size: 24, color: Colors.white),
            activeIcon: Icon(Icons.account_balance, size: 24, color: Color.fromRGBO(58, 81, 153, 1.0)),
            title: Text(""))
      ],
      currentIndex: _selectedIndex,
      onTap: _onItemTapped,
    );
  }
}

