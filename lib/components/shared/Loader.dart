import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:loading_animations/loading_animations.dart';

class Loader extends StatelessWidget {
  final Color color;

  Loader({  Key key, Color color }): this.color = color == null ? Color.fromRGBO(90, 130, 223, 1.0) : color, super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: LoadingBouncingGrid.circle(
        size: 60,
        backgroundColor: this.color,
      ),
    );
  }

}