import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/components/shared/SearchButton.dart';

class Header extends StatelessWidget implements PreferredSizeWidget {
  final String title;

  const Header({Key key, this.title}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      backgroundColor: Color.fromRGBO(50, 88, 178, 1),
      leading: BackButton(),
      title: Text(
        this.title,
        style: TextStyle(
            fontSize: 18, color: Colors.white, fontWeight: FontWeight.bold
        ),
      ),
      actions: <Widget>[
        SearchButton(),
      ],
    );
  }

  @override
  Size get preferredSize => Size.fromHeight(60);
}
