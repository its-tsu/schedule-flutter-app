import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ConnectionError extends StatelessWidget {
  final Color iconColor;

  ConnectionError({Key key, Color iconColor})
      : this.iconColor =
            iconColor == null ? Color.fromRGBO(90, 130, 223, 1.0) : iconColor,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      flex: 1,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Icon(Icons.signal_wifi_off, size: 50, color: this.iconColor),
            Text(
                "Не удалось получить данные",
              style: TextStyle(
                fontSize: 16
              ),
            )
          ],
        )
    );
  }
}
