import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class SearchButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return IconButton(
      onPressed: () {
        Navigator.pushNamed(context, "/search");
      },
      icon: Icon(
          Icons.search,
        size: 24,
        color: Colors.white,
      ),
    );
  }

}