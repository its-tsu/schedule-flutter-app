import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/components/shared/ConnectionError.dart';
import 'package:flutter_app/components/shared/Loader.dart';
import 'package:flutter_app/data/ResponseTypesEnum.dart';
import 'package:flutter_app/models/BaseModel.dart';
import 'package:flutter_app/models/screen-arguments/ItemsViewArguments.dart';
import 'package:flutter_app/services/api/RootItemsApiService.dart';

class RootItemsList extends StatefulWidget {
  final ResponseTypesEnum type;

  const RootItemsList({Key key, this.type}) : super(key: key);

  _RootItemsListState createState() => _RootItemsListState(type: this.type);
}

class _RootItemsListState extends State<RootItemsList> {
  final ResponseTypesEnum type;
  RootItemsApiService rootItemsApiService;
  Future<List<BaseModel>> futureList;

  _RootItemsListState({this.type}) {
    this.rootItemsApiService = RootItemsApiService();
  }

  @override
  void initState() {
    super.initState();
    futureList = this.rootItemsApiService.getRootItemsListByType(type);
  }

  @override
  Widget build(BuildContext context) {
    String path = ModalRoute.of(context).settings.name;

    return FutureBuilder<List<BaseModel>>(
        future: this.futureList,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return ListView.builder(
                padding: EdgeInsets.only(bottom: 60.0),
                itemCount: snapshot.data.length,
                itemBuilder: (context, index) {
                  return Container(
                    margin: EdgeInsets.only(top: 20.0),
                    child: OutlineButton(
                        onPressed: () {
                          Navigator.pushNamed(context, path + "/items",
                              arguments: ItemsViewArguments(
                                  snapshot.data[index].id, this.type));
                        },
                        color: Colors.white,
                        padding: EdgeInsets.only(left: 0.0, right: 0.0),
                        child: Container(
                          child: Text(
                            snapshot.data[index].name,
                            textAlign: TextAlign.left,
                            style: TextStyle(
                              color: Colors.black,
                            ),
                          ),
                          color: Colors.white,
                          padding: EdgeInsets.all(15.0),
                          width: double.infinity,
                        )),
                  );
                });
          }

          if (snapshot.hasError) {
            return ConnectionError();
          }

          return Loader();
        });
  }
}
