class Api {
  static final String root = "https://its.tltsu.ru/api";
  static final String buildings = root + "/buildings";
  static final String institutes = root + "/institutes";

  static final String floors = root + "/floors/building/{buildingId}";
  static final String departments = root + "/departments/institute/{instituteId}";
  static final String courses = root + "/courses/institute/{instituteId}";

  static final String classrooms = root + "/classrooms/building/{buildingId}/floor/{floorId}";
  static final String teachers = root + "/teachers/department/{departmentId}";
  static final String groups = root + "/groups/course/{course}/institute/{instituteId}";

  static final String groupSchedule = root + "/schedule/group?groupId={groupId}&fromDate={fromDate}&toDate={toDate}";
  static final String teacherSchedule = root + "/schedule/teacher?teacherId={teacherId}&fromDate={fromDate}&toDate={toDate}";
  static final String classroomSchedule = root + "/schedule/classroom?classroomId={classroomId}&fromDate={fromDate}&toDate={toDate}";

  static final String teacherById = root + "/teachers/";

  static final String search = root + "/search?name=";
}