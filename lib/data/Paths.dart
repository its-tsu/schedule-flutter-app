class Paths {
  static final String studentsRoot = "/students";
  static final String teachersRoot = "/teachers";
  static final String classroomsRoot = "/classrooms";

  static final String students = studentsRoot + "/items";
  static final String teachers = teachersRoot + "/items";
  static final String classrooms = classroomsRoot + "/items";

  static final String studentsSchedule = studentsRoot + "/schedule";
  static final String teachersSchedule = teachersRoot + "/schedule";
  static final String classroomsSchedule = classroomsRoot + "/schedule";
}