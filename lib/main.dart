import 'package:flutter/material.dart';
import 'package:flutter_app/data/Paths.dart';
import 'package:flutter_app/views/ItemsView.dart';
import 'package:flutter_app/views/MainView.dart';
import 'package:flutter_app/views/RootItemsView.dart';
import 'package:flutter_app/views/ScheduleView.dart';
import 'package:flutter_app/views/SearchView.dart';

void main() {
  runApp(ScheduleTSU());
}

class ScheduleTSU extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Расписание ТГУ',
      initialRoute: '/',
      routes: {
        "/": (context) => MainView(),
        "/search": (context) => SearchView(),

        Paths.studentsRoot: (context) => RootItemsView(),
        Paths.teachersRoot: (context) => RootItemsView(),
        Paths.classroomsRoot: (context) => RootItemsView(),

        Paths.students: (context) => ItemsView(),
        Paths.teachers: (context) => ItemsView(),
        Paths.classrooms: (context) => ItemsView(),

        Paths.studentsSchedule: (context) => ScheduleView(),
        Paths.teachersSchedule: (context) => ScheduleView(),
        Paths.classroomsSchedule: (context) => ScheduleView(),
      },
    );
  }
}
