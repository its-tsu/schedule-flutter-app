import 'dart:convert';

import 'package:flutter_app/data/Api.dart';
import 'package:flutter_app/data/ResponseTypesEnum.dart';
import 'package:flutter_app/models/ClassModel.dart';
import 'package:http/http.dart' as http;

class ScheduleApiService {

  Future<List<List<ClassModel>>> getScheduleByTypeAndDates({
    ResponseTypesEnum type,
    int itemId,
    String fromDate,
    String toDate
  }) async {
    http.Response scheduleResponse;

    if (type == ResponseTypesEnum.students) {
      scheduleResponse = await this._getScheduleForGroup(itemId, fromDate, toDate);
    }

    if (type == ResponseTypesEnum.teachers) {
      scheduleResponse = await this._getScheduleForTeacher(itemId, fromDate, toDate);
    }

    if (type == ResponseTypesEnum.classRooms) {
      scheduleResponse = await this._getScheduleForClassroom(itemId, fromDate, toDate);
    }

    List<ClassModel> convertedList;

    if (scheduleResponse.statusCode == 200) {
      var responseList = jsonDecode(scheduleResponse.body) as List;

      if (responseList == null) {
        throw Exception('Не удалось загрузить данные');
      }

      convertedList = responseList.map((e) => ClassModel.fromJson(e)).toList();

    } else {
      throw Exception('Не удалось загрузить данные');
    }

    return this._getListToDisplay(convertedList);
  }

  Future<http.Response> _getScheduleForGroup(
      int groupId, String fromDate, String toDate) {
    return http.get(Api.groupSchedule
        .replaceAll("{groupId}", groupId.toString())
        .replaceAll("{fromDate}", fromDate)
        .replaceAll("{toDate}", toDate));
  }

  Future<http.Response> _getScheduleForTeacher(
      int teacherId, String fromDate, String toDate) {
    return http.get(Api.teacherSchedule
        .replaceAll("{teacherId}", teacherId.toString())
        .replaceAll("{fromDate}", fromDate)
        .replaceAll("{toDate}", toDate));
  }

  Future<http.Response> _getScheduleForClassroom(
      int classroomId, String fromDate, String toDate) {
    return http.get(Api.classroomSchedule
        .replaceAll("{classroomId}", classroomId.toString())
        .replaceAll("{fromDate}", fromDate)
        .replaceAll("{toDate}", toDate));
  }

  List<List<ClassModel>> _getListToDisplay(List<ClassModel> list) {
    List<List<ClassModel>> listTemplate = [[], [], [], [], [], [], [], []];

    list.forEach((element) {
      listTemplate[element.pairNumber - 1].add(element);
    });

    int index = listTemplate[0].length > 0 ? 0 : 3;

    for (var i = 7; i > 0; i--) {
      if (listTemplate[i].length > 0) {
        index = i;
        break;
      }
    }

    List<List<ClassModel>> result = [];

    for (var i = 0; i <= index; i++) {
      result.add(listTemplate[i]);
    }

    return result;
  }
}
