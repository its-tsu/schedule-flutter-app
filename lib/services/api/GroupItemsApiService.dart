import 'dart:convert';

import 'package:flutter_app/data/Api.dart';
import 'package:flutter_app/data/ResponseTypesEnum.dart';
import 'package:flutter_app/models/BaseModel.dart';
import 'package:flutter_app/models/GroupItems.dart';
import 'package:flutter_app/models/Teacher.dart';
import 'package:http/http.dart' as http;

class GroupItemsApiService {

  Future<List<GroupItems>> getGroups(rootItemId, ResponseTypesEnum type) async {
    http.Response groupsResponse;

    if (type == ResponseTypesEnum.students) {
      groupsResponse = await this._getCoursesByInstituteId(rootItemId);
    }

    if (type == ResponseTypesEnum.teachers) {
      groupsResponse = await this._getDepartmentsByInstituteId(rootItemId);
    }

    if (type == ResponseTypesEnum.classRooms) {
      groupsResponse = await this._getFloorsByBuildingId(rootItemId);
    }

    if (groupsResponse == null) throw Exception("Не удалось определить тип запроса");

    List<BaseModel> groupsList = [];

    if (groupsResponse.statusCode == 200) {
      var responseList = jsonDecode(groupsResponse.body) as List;

      if (responseList == null) {
        return [];
      }

      if (type == ResponseTypesEnum.teachers) {
        groupsList = responseList.map((e) => BaseModel.fromJson(e)).toList();
      } else {
        responseList.forEach((element) {
          groupsList.add(BaseModel(id: 0, name: element));
        });
      }

    } else {
      throw Exception('Не удалось загрузить данные');
    }

    List<GroupItems> result = [];

    await Future.forEach(groupsList, (element) async {
      var itemsList = await this._getItemsListByRootItem(
          type, element.id == 0 ? element.name : element.id, rootItemId
      );

      result.add(GroupItems(group: element, itemsList: itemsList));
    });

    result.sort((a, b) => a.group.name.compareTo(b.group.name));

    return result;
  }

  Future<List<BaseModel>> _getItemsListByRootItem(ResponseTypesEnum type, groupItemId, int rootItemId) async {
    http.Response itemsResponse;

    if (type == ResponseTypesEnum.students) {
      itemsResponse = await this._getGroupsByInstituteAndCourse(rootItemId, groupItemId);
    }

    if (type == ResponseTypesEnum.teachers) {
      itemsResponse = await this._getTeachersByDepartment(groupItemId);
    }

    if (type == ResponseTypesEnum.classRooms) {
      itemsResponse = await this._getClassroomsByBuildingAndFloor(rootItemId, groupItemId);
    }

    if (itemsResponse.statusCode == 200) {
      var responseList = jsonDecode(itemsResponse.body) as List;

      if (responseList == null) {
        return [];
      }

      var convertedResponse = [];

      if (type == ResponseTypesEnum.teachers) {
        var teachersList = responseList.map((e) => Teacher.fromJson(e));
        convertedResponse = teachersList.map((e) => BaseModel(id: e.id, name: e.lastName + " " + e.name + " " + e.patronymic)).toList();
      } else {
        convertedResponse = responseList.map((e) => BaseModel.fromJson(e)).toList();
      }

      convertedResponse.sort((a, b) => a.name.compareTo(b.name));

      return convertedResponse;

    } else {
      return [];
    }

  }

  Future<http.Response> _getCoursesByInstituteId(int instituteId) {
    return http.get(Api.courses.replaceAll("{instituteId}", instituteId.toString()));
  }

  Future<http.Response> _getDepartmentsByInstituteId(int instituteId) {
    return http.get(Api.departments.replaceAll("{instituteId}", instituteId.toString()));
  }

  Future<http.Response> _getFloorsByBuildingId(int buildingId) {
    return http.get(Api.floors.replaceAll("{buildingId}", buildingId.toString()));
  }

  Future<http.Response> _getGroupsByInstituteAndCourse(int instituteId, String course) {
    return http.get(Api.groups.replaceAll("{instituteId}", instituteId.toString()).replaceAll("{course}", course));
  }

  Future<http.Response> _getTeachersByDepartment(int departmentId) {
    return http.get(Api.teachers.replaceAll("{departmentId}", departmentId.toString()));
  }

  Future<http.Response> _getClassroomsByBuildingAndFloor(int buildingId, String floor) {
    return http.get(Api.classrooms.replaceAll("{buildingId}", buildingId.toString()).replaceAll("{floorId}", floor));
  }
}