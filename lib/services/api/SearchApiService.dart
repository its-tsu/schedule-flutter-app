import 'dart:convert';

import 'package:flutter_app/data/Api.dart';
import 'package:flutter_app/models/SearchItem.dart';
import 'package:http/http.dart' as http;

class SearchApiService {

  Future<List<SearchItem>> getAllItemsByName(String name) async {
    if (name == "") {
      return [];
    }

    http.Response response = await http.get(Api.search + name);

    if (response.statusCode != 200) {
      throw Exception('Не удалось загрузить данные');
    }

    var responseList = jsonDecode(response.body) as List;

    if (responseList == null) {
      throw Exception('Не удалось загрузить данные');
    }

    var result = responseList.map((e) => SearchItem.fromJson(e)).toList();
    result.sort((a, b) => a.name.compareTo(b.name));

    return result;
  }
}