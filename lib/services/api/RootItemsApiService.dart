import 'dart:convert';

import 'package:flutter_app/data/Api.dart';
import 'package:flutter_app/data/ResponseTypesEnum.dart';
import 'package:flutter_app/models/BaseModel.dart';
import 'package:http/http.dart' as http;

class RootItemsApiService {
  Future<List<BaseModel>> getRootItemsListByType(ResponseTypesEnum responseType) async {
    http.Response response;

    if (responseType == ResponseTypesEnum.students ||
        responseType == ResponseTypesEnum.teachers) {
      response = await _getInstitutesList();
    }

    if (responseType == ResponseTypesEnum.classRooms) {
      response = await _getBuildingsList();
    }

    if (response.statusCode == 200) {
      var responseList = jsonDecode(response.body) as List;

      if (responseList == null) {
        return [];
      }

      var convertedResponse = responseList.map((e) => BaseModel.fromJson(e)).toList();
      convertedResponse.sort((a, b) => a.name.compareTo(b.name));

      return convertedResponse;

    } else {
      throw Exception('Не удалось загрузить данные');
    }
  }

  Future<http.Response> _getInstitutesList() {
    return http.get(Api.institutes);
  }

  Future<http.Response> _getBuildingsList() {
    return http.get(Api.buildings);
  }
}
