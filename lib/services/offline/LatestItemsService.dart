import 'dart:convert';
import 'package:flutter_app/data/Api.dart';
import 'package:flutter_app/models/Teacher.dart';
import 'package:http/http.dart' as http;

import 'package:flutter_app/models/LatestItemsModel.dart';
import 'package:flutter_app/services/offline/FileService.dart';

class LatestItemsService {
  FileService _fileService;

  LatestItemsService() {
    this._fileService = FileService("latest-items.json");
  }

  Future<void> updateItems(LatestItemsModel item) async {
    List<LatestItemsModel> itemsList = await this.getLatestItems();
    var index = itemsList.indexWhere((element) => element.id == item.id && element.typeString == item.typeString);

    if (index >= 0) {
      itemsList[index].lastVisitDate = DateTime.now().toIso8601String();
    } else {
      if (item.typeString == "teachers") {
        var teacher = await this._getTeacherById(item.id);
        item.name = "${teacher.lastName} ${teacher.name[0]}.${teacher.patronymic[0]}.";
      }

      if (itemsList.length >= 6) {
        itemsList[5] = item;
      } else {
        itemsList.add(item);
      }
    }

    itemsList.sort((a, b) => b.lastVisitDate.compareTo(a.lastVisitDate));
    this._fileService.writeJsonToFile(jsonEncode(itemsList));
  }

  Future<List<LatestItemsModel>> getLatestItems() async {
    var fileString = await this._readItems();
    var itemsListJson = jsonDecode(fileString) as List;
    return itemsListJson.map((e) => LatestItemsModel.fromJson(e)).toList();
  }

  Future<String> _readItems() async {
    return await this._fileService.readJsonFromFile();
  }

  Future<Teacher> _getTeacherById(int id) async {
    var response = await http.get(Api.teacherById + id.toString());

    if (response.statusCode == 200) {
      return Teacher.fromJson(jsonDecode(response.body));
    } else {
      throw Exception("Не удалось получить преподавателя");
    }
  }
}