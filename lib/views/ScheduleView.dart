import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/components/schedule/ScheduleList.dart';
import 'package:flutter_app/components/shared/Header.dart';
import 'package:flutter_app/components/shared/SharedBottomNavigationBar.dart';
import 'package:flutter_app/data/Paths.dart';
import 'package:flutter_app/models/screen-arguments/ScheduleViewArguments.dart';

class ScheduleView extends StatelessWidget {

  String _getTitleByUrl(BuildContext context) {
    String path = ModalRoute.of(context).settings.name;

    if (path == Paths.studentsSchedule) {
      return "Расписание студентов";
    }

    if (path == Paths.teachersSchedule) {
      return "Расписание преподавателей";
    }

    if (path == Paths.classroomsSchedule) {
      return "Расписание аудиторий";
    }

    return "";
  }

  @override
  Widget build(BuildContext context) {
    ScheduleViewArguments args = ModalRoute.of(context).settings.arguments;

    return Scaffold(
      appBar: Header(
        title: _getTitleByUrl(context),
      ),
      body: Container(
        child: ScheduleList(args),
      ),
      bottomNavigationBar: SharedBottomNavigationBar(),
    );
  }

}