import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/components/main/LatestItemsList.dart';
import 'package:flutter_app/components/main/MainHeader.dart';
import 'package:flutter_app/components/main/MainLink.dart';
import 'package:flutter_app/data/Paths.dart';

class MainView extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: Color(0xFFFFFFFF),
        child: Column(
          children: <Widget>[
            Container(
              decoration: BoxDecoration(
                color: Color.fromRGBO(50, 88, 178, 1),
              ),
              height: 270.0,
              child: Container(
                margin: EdgeInsets.only(top: 30),
                child: Column(
                  children: <Widget>[
                    Expanded(
                      flex: 1,
                      child: MainHeader()
                    ),
                    Expanded(
                      flex: 3,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          LatestItemsList()
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ),
            Container(
              constraints: BoxConstraints(maxWidth: 520),
              padding: EdgeInsets.only(top: 50.0, left: 20.0, right: 20.0),
              width: double.infinity,
              child: Wrap(
                alignment: WrapAlignment.spaceBetween,
                children: <Widget>[
                  MainLink(
                    icon: Icons.school,
                    text: "Студенты",
                    link: Paths.studentsRoot,
                  ),
                  MainLink(
                    icon: Icons.collections_bookmark,
                    text: "Преподаватели",
                    link: Paths.teachersRoot,
                  ),
                  MainLink(
                    icon: Icons.account_balance,
                    text: "Аудитории",
                    link: Paths.classroomsRoot,
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
