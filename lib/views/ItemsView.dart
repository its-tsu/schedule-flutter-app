import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/components/items-list/ItemsList.dart';
import 'package:flutter_app/components/shared/Header.dart';
import 'package:flutter_app/components/shared/SharedBottomNavigationBar.dart';
import 'package:flutter_app/data/Paths.dart';
import 'package:flutter_app/models/screen-arguments/ItemsViewArguments.dart';

class ItemsView extends StatelessWidget {

  String _getTitleByUrl(BuildContext context) {
    String path = ModalRoute.of(context).settings.name;

    if (path == Paths.students) {
      return "Группы";
    }

    if (path == Paths.teachers) {
      return "Преподаватели";
    }

    if (path == Paths.classrooms) {
      return "Аудитории";
    }

    return "";
  }

  @override
  Widget build(BuildContext context) {
    ItemsViewArguments args = ModalRoute.of(context).settings.arguments;

    return Scaffold(
      appBar: Header(
        title: _getTitleByUrl(context),
      ),
      body: Container(
        color: Color.fromRGBO(234, 240, 255, 1.0),
        padding: EdgeInsets.only(left: 20.0, right: 20.0),
        child: Container(
          child: ItemsList(type: args.type, rootItemId: args.id),
        ),
      ),
      bottomNavigationBar: SharedBottomNavigationBar(),
    );
  }

}