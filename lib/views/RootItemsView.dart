import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/components/root-items/RootItemsList.dart';
import 'package:flutter_app/components/shared/Header.dart';
import 'package:flutter_app/components/shared/SharedBottomNavigationBar.dart';
import 'package:flutter_app/data/Paths.dart';
import 'package:flutter_app/data/ResponseTypesEnum.dart';

class RootItemsView extends StatelessWidget {

  String _getTitleByUrl(BuildContext context) {
    String path = ModalRoute.of(context).settings.name;

    if (path == Paths.studentsRoot || path == Paths.teachersRoot) {
      return "Выберите институт";
    }

    if (path == Paths.classroomsRoot) {
      return "Выберите корпус";
    }

    return "";
  }

  ResponseTypesEnum _getTypeByUrl(BuildContext context) {
    String path = ModalRoute.of(context).settings.name;

    if (path == Paths.teachersRoot) {
      return ResponseTypesEnum.teachers;
    }

    if (path == Paths.classroomsRoot) {
      return ResponseTypesEnum.classRooms;
    }

    return ResponseTypesEnum.students;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: Header(
        title: _getTitleByUrl(context),
      ),
      body: Container(
        color: Color.fromRGBO(234, 240, 255, 1.0),
        padding: EdgeInsets.only(left: 20.0, right: 20.0),
        child: RootItemsList(type: this._getTypeByUrl(context)),
      ),
      bottomNavigationBar: SharedBottomNavigationBar(),
    );
  }

}