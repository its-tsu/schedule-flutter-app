import 'package:flutter_app/data/ResponseTypesEnum.dart';
import 'package:flutter_app/models/BaseModel.dart';

class LatestItemsModel extends BaseModel {
  String typeString;
  String lastVisitDate;
  String lastUpdatedDate;

  LatestItemsModel(
      {int id,
      String name,
      this.typeString,
      this.lastUpdatedDate,
      this.lastVisitDate})
      : super(id: id, name: name);

  Map toJson() => {
    "id": id,
    "name": name,
    "type": typeString,
    "lastVisitDate": lastVisitDate,
    "lastUpdatedDate": lastUpdatedDate
  };

  factory LatestItemsModel.fromJson(Map<String, dynamic> json) {
    return LatestItemsModel(
      id: json["id"],
      name: json["name"],
      typeString: json["type"],
      lastVisitDate: json["lastVisitDate"],
      lastUpdatedDate: json["lastUpdatedDate"]
    );
  }

  static String getType(ResponseTypesEnum type) {
    if (type == ResponseTypesEnum.students) {
      return "students";
    }

    if (type == ResponseTypesEnum.teachers) {
      return "teachers";
    }

    if (type == ResponseTypesEnum.classRooms) {
      return "classRooms";
    }

    return "";
  }
}
