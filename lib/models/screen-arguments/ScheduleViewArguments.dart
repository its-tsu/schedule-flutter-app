import 'package:flutter_app/data/ResponseTypesEnum.dart';
import 'package:flutter_app/models/BaseModel.dart';

class ScheduleViewArguments {
  BaseModel item;
  ResponseTypesEnum type;

  ScheduleViewArguments({ this.item, this.type });
}