import 'package:flutter_app/data/ResponseTypesEnum.dart';

class ItemsViewArguments {
  final int id;
  final ResponseTypesEnum type;

  ItemsViewArguments(this.id, this.type);
}