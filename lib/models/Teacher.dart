import 'package:flutter_app/models/BaseModel.dart';

class Teacher extends BaseModel {
  String lastName;
  String patronymic;

  Teacher({ int id, String name, this.lastName, this.patronymic }): super(id: id, name: name);

  factory Teacher.fromJson(Map<String, dynamic> json) {
    return Teacher(
      id: json["id"],
      name: json["name"],
      lastName: json["lastName"],
      patronymic: json["patronymic"]
    );
  }
}