import 'BaseModel.dart';
import 'Teacher.dart';

class ClassModel {
  String id;
  String disciplineName;
  String type;
  int pairNumber;
  String date;
  String fromTime;
  String toTime;
  Teacher teacher;
  BaseModel classroom;
  List<BaseModel> groupsList;

  ClassModel({
    this.id,
    this.disciplineName,
    this.type,
    this.pairNumber,
    this.date,
    this.fromTime,
    this.toTime,
    this.teacher,
    this.classroom,
    this.groupsList
  });

  factory ClassModel.fromJson(Map<String, dynamic> json) {
    var groupsList = json["groupsList"] as List;

    return ClassModel(
      id: json["id"],
      disciplineName: json["disciplineName"],
      type: json["type"],
      pairNumber: json["pairNumber"],
      date: json["date"],
      fromTime: json["fromTime"],
      toTime: json["toTime"],
      teacher: json["teacher"] != null ? Teacher.fromJson(json["teacher"]) : null,
      classroom: json["classroom"] != null ? BaseModel.fromJson(json["classroom"]) : null,
      groupsList: groupsList.map((e) => BaseModel.fromJson(e)).toList()
    );
  }
}
