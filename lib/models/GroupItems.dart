import 'package:flutter_app/models/BaseModel.dart';

class GroupItems {
  BaseModel group;
  List<BaseModel> itemsList;

  GroupItems({ this.group, this.itemsList });
}