class BaseModel {
  int id;
  String name;

  BaseModel({ this.id, this.name });

  factory BaseModel.fromJson(Map<String, dynamic> json) {
    return BaseModel(
      id: json["id"],
      name: json["name"]
    );
  }
}