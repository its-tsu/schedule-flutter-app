import 'package:flutter_app/data/ResponseTypesEnum.dart';
import 'package:flutter_app/models/BaseModel.dart';

class SearchItem extends BaseModel {
  ResponseTypesEnum type;

  SearchItem({ int id, String name, this.type }): super(id: id, name: name);

  factory SearchItem.fromJson(Map<String, dynamic> json) {
    return SearchItem(
      id: json["id"],
      name: json["name"],
      type: _getTypeByName(json["type"])
    );
  }

  static ResponseTypesEnum _getTypeByName(String typeString) {
    if (typeString == "group") {
      return ResponseTypesEnum.students;
    }

    if (typeString == "teacher") {
      return ResponseTypesEnum.teachers;
    }

    if (typeString == "classroom") {
      return ResponseTypesEnum.classRooms;
    }

    return ResponseTypesEnum.students;
  }
}