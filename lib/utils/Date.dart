import 'package:intl/intl.dart';
import 'package:intl/date_symbol_data_local.dart';

class Date {

  static void initLocale() {
    initializeDateFormatting();
  }

  static String getWeekDay(DateTime date) {
    return DateFormat("EE", "ru").format(date);
  }

  static String getMonth(DateTime date) {
    return DateFormat("MMMM", "ru").format(date);
  }

  static bool isTheSame(DateTime a, DateTime b) {
    return a.year == b.year && a.month == b.month && a.day == b.day;
  }

  static String getTimeFromStringDate(String dateString) {
    var dateValue = new DateFormat("yyyy-MM-ddTHH:mm:ss.sssZ").parseUTC(dateString).toLocal();
    return DateFormat("HH:mm").format(dateValue);
  }

  static String getDateStringForRequest(DateTime date) {
    var isoDate = date.toIso8601String();
    var dateString = isoDate.split("T")[0];

    return dateString + "T12:00:00.000Z";
  }
}